// @ts-check

// @ts-ignore
const assert = require('assert');
// @ts-ignore
const testing_lib = require('./../index.js')()
const lib_path = require('path')
const lib_fs = require('fs')

let path = lib_path.join(__dirname, 'translate')

describe("translate", function() {
    if (lib_fs.existsSync(path)) {
        lib_fs.readdirSync(path).forEach(file => {
            lib_fs.unlinkSync(lib_path.join(path, file))
        })
        lib_fs.rmdirSync(path)
    }

    it('init, check path', function() {
        assert.equal(testing_lib.init('eng', path, "{{", "}}", 0), path)
    })

    it('text1 with empty dictionary, eng', function() {
        assert.equal(testing_lib.translate('eng','{{0}}, you have read page(s) {{1}}, total pages {{2}}!',['Ann', '16', '42']), 'Ann, you have read page(s) 16, total pages 42!')
    })
    it('text1 with empty dictionary, rus', function() {
        assert.equal(testing_lib.translate('rus','{{0}}, you have read page(s) {{1}}, total pages {{2}}!',['Ann', '16', '42']), 'Ann, you have read page(s) 16, total pages 42!')
    })
    it('text2 with empty dictionary, rus', function() {
        assert.equal(testing_lib.translate('rus','hi'), 'hi')
    })
    it('ext translate: text1 with empty dictionary, eng', function() {
        assert.equal(testing_lib.translateExt('eng','{{Ann}}, you have read page(s) {{25}}, total pages {{52}}!'), 'Ann, you have read page(s) 25, total pages 52!')
    })
    it('ext translate: text1 with empty dictionary, rus', function() {
        assert.equal(testing_lib.translateExt('rus','{{Ann}}, you have read page(s) {{25}}, total pages {{52}}!'), 'Ann, you have read page(s) 25, total pages 52!')
    })
    it('ext translate: text2 with empty dictionary, rus', function() {
        assert.equal(testing_lib.translateExt('rus','hi'), 'hi')
    })

    it('fill dictionary', function() {
        lib_fs.writeFileSync(
            lib_path.join(path, 'translate.rus.json'),
            JSON.stringify(
                [
                    {
                        key: "{{0}}, you have read page(s) {{1}}, total pages {{2}}!",
                        dirty: "{{0}}, вы прочитали страниц {{1}}, всего страниц {{2}}!",
                    },
                    {
                        key: "hi",
                        quality: "Привет"
                    }
                ],
                null,
                '\t'
            )
        )
    })
    it('text1 with filled dictionary, eng', function() {
        assert.equal(testing_lib.translate('eng','{{0}}, you have read page(s) {{1}}, total pages {{2}}!',['Ann', '16', '42']), 'Ann, you have read page(s) 16, total pages 42!')
    })
    it('text1 with filled dictionary, rus', function() {
        assert.equal(testing_lib.translate('rus','{{0}}, you have read page(s) {{1}}, total pages {{2}}!',['Ann', '16', '42']), 'Ann, вы прочитали страниц 16, всего страниц 42!')
    })
    it('text2 with filled dictionary, rus', function() {
        assert.equal(testing_lib.translate('rus','hi'), 'Привет')
    })
    it('ext translate: text1 with filled dictionary, eng', function() {
        assert.equal(testing_lib.translateExt('eng','{{Ann}}, you have read page(s) {{35}}, total pages {{77}}!'), 'Ann, you have read page(s) 35, total pages 77!')
    })
    it('ext translate: text1 with filled dictionary, rus', function() {
        assert.equal(testing_lib.translateExt('rus','{{Ann}}, you have read page(s) {{35}}, total pages {{77}}!'), 'Ann, вы прочитали страниц 35, всего страниц 77!')
    })
    it('ext translate: text2 with filled dictionary, rus', function() {
        assert.equal(testing_lib.translateExt('rus','hi'), 'Привет')
    })
})