// 1. run this example
// 2. find file /translate/translate.rus.json and edit fields "dirty" or/and "quality"
// 3. run this example again

let t = require('./index.js')()

t.on('error', error => {
    console.log('error', error)
})

t.on('no_translate', o => {
    console.log('no_translate', o.language, o.text)
})

let path_with_language = t.init('eng')
console.log(path_with_language)
console.log(t.translateExt('eng','You have read page(s) {0}, total page(s) {1}'))
console.log(t.translateExt('rus','You have read page(s) {0}, total page(s) {1}'))
console.log(t.translateExt('rus','hi!'))