// @ts-check
/**
 * @license MIT
 * @author Vitalii vasilev
 */

/**
* @private
* @typedef type_store
* @property {string} language
* @property {type_store_text[]} text
*/

/**
* @private
* @typedef type_store_text
* @property {string} key
* @property {string} dirty
* @property {string} quality
* @property {string} last_read_datetime
* @property {string[]} tags
*/

/** @private */
const lib_util = require('util')
/** @private */
const lib_event = require('events').EventEmitter
/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_fs = require('fs')
/** @private */
const lib_path = require('path')

module.exports = Translator
lib_util.inherits(Translator, lib_event)

/**
* @class  (license MIT) simple translator, full example - see example.js
*/
function Translator() {
    if (!(this instanceof Translator)) return new Translator()
    lib_event.call(this)
}

/** @private
 * @type {any} */
Translator.prototype.emit = Translator.prototype.emit || undefined
/** @private
 * @type {string} */
Translator.prototype.path = undefined
/** @private
 * @type {string} */
Translator.prototype.left = undefined
/** @private
 * @type {string} */
Translator.prototype.right = undefined
/** @private
 * @type {string} */
Translator.prototype.language = undefined
/** @private
 * @type {type_store[]} */
Translator.prototype.store = undefined
/** @private
 * @type {any} */
Translator.prototype.on = Translator.prototype.on || undefined

/**
 * initialization translator, set root path for storage text translate files
 * @param {string} language base (main) language
 * @param {string} [path] root path for text translate files, if empty - set __dirname/translate
 * @param {string} [left] left border for mark non-translate substring, if empty - "{"
 * @param {string} [right] right border for mark non-translate substring, if empty - "}"
 * @param {number} [timeout_save] timeout (in minutes) save translate data to disk, need for save last_read_datetime, default - "60"
 * @returns {string} full path name for storage text translate files
 */
Translator.prototype.init = function (language, path, left, right, timeout_save) {
    try {
        if (lib_vconv.isEmpty(language)) {
            throw new Error('base language is empty')
        }

        if (lib_vconv.isEmpty(path)) {
            path = lib_path.join(__dirname,'translate')
        } else {
            if (lib_vconv.isEmpty(lib_path.dirname(path))) {
                path = lib_path.join(__dirname, path)
            }
        }
        this.left = lib_vconv.toString(left,'{')
        this.right = lib_vconv.toString(right,'}')
        this.store = []
        createPath(path)
        lib_fs.readdirSync(path).forEach(file => {
            let pos1 = file.indexOf('translate.')
            let pos2 = file.lastIndexOf('.json')
            if (pos1 === 0 && pos2 > 10) {
                let language = file.substring(10, pos2)
                if (!lib_vconv.isEmpty(language)) {
                    this.store.push(load(path, language))
                }
            }
        })

        this.path = path
        this.language = language

        go_timeout_save(this, lib_vconv.toInt(timeout_save, 60))

        return this.path
    } catch(error) {
        this.path = undefined
        this.language = undefined
        this.store = undefined
        throw lib_vconv.toErrorMessage(error, 'viva-translate.init("{0}")',path)
    }
}

/**
 * translate text
 * @param {string} language translate to this language
 * @param {string} text text for translate
 * @param {string|string[]} [replaces] substitutions
 * @param {string|string[]} [tags] tags for phrase
 * @returns {string}
 */
Translator.prototype.translate = function (language, text, replaces, tags) {
    try {
        if (lib_vconv.isAbsent(this.language)) {
            throw new Error ('language is empty')
        }
        if (lib_vconv.isAbsent(this.store)) {
            throw new Error ('language storage is empty, probably no call "init"')
        }

        if (language.toLowerCase() !== this.language.toLowerCase()) {
            this.translate(this.language, text, replaces, tags)
        }

        let tags_arr = get_tags(tags)

        let find_store = this.store.find(f => f.language.toLowerCase() === language.toLowerCase())
        if (lib_vconv.isAbsent(find_store)) {
            find_store = load(this.path, language)
            this.store.push(find_store)
        }

        let find_text = find_store.text.find(f => f.key.toLowerCase() === text.toLowerCase())
        if (lib_vconv.isAbsent(find_text)) {
            find_store = load(this.path, language)
            let find_text = find_store.text.find(f => f.key.toLowerCase() === text.toLowerCase())
            if (lib_vconv.isAbsent(find_text)) {
                find_text = {
                    key: text,
                    dirty: '',
                    quality: '',
                    last_read_datetime: lib_vconv.formatDate(new Date(), 126),
                    tags: tags_arr
                }
                find_store.text.push(find_text)
                save(this.path, find_store, (this.language === find_store.language ? true : false))
                if (language.toLowerCase() !== this.language.toLowerCase()) {
                    this.emit('no_translate',{language: language, text: text})
                }
                return format(find_text, replaces, this.left, this.right)
            } else {
                add_tags(find_text, tags_arr)
                find_text.last_read_datetime = lib_vconv.formatDate(new Date(), 126)
                return format(find_text, replaces, this.left, this.right)
            }
        } else {
            find_text.last_read_datetime = lib_vconv.formatDate(new Date(), 126)
            add_tags(find_text, tags_arr)
            return format(find_text, replaces, this.left, this.right)
        }
    } catch (error) {
        this.emit('error', error)
        return lib_vconv.format(text, replaces)
    }
}

/**
 * translate text with auto-check replaces
 * @param {string} language translate to this language
 * @param {string} text text for translate
 * @param {string|string[]} [tags] tags for phrase
 * @returns {string}
 */
Translator.prototype.translateExt = function (language, text, tags) {
    try {
        let t = lib_vconv.toString(text,'')
        let true_text = ''
        let true_replaces = []

        let index_left = t.indexOf(this.left)
        let index_right = t.indexOf(this.right, index_left)
        let index_worked_out = 0

        while (index_left >= 0 && index_right >= 0) {
            true_text = true_text + t.substring(index_worked_out, index_left) + this.left + true_replaces.length.toString() + this.right

            true_replaces.push(t.substring(index_left + this.left.length, index_right))

            index_worked_out = index_right + this.right.length
            index_left = t.indexOf(this.left, index_worked_out)
            index_right = t.indexOf(this.right, index_left)
        }

        if (t.length > index_worked_out) {
            true_text = true_text + t.substring(index_worked_out, t.length)
        }

        return this.translate(language, true_text, true_replaces, tags)
    } catch (error) {
        this.emit('error', error)
        return text
    }
}

/**
 * find base language text by text in secondary language
 * @param {string} language
 * @param {string} text
 * @returns {string}
 */
Translator.prototype.translate_back = function (language, text) {
    let find_store = this.store.find(f => f.language.toLowerCase() === language.toLowerCase())
    if (lib_vconv.isAbsent(find_store)) return undefined

    let find_text = find_store.text.find(f => f.quality === text)
    if (lib_vconv.isAbsent(find_text)) {
        find_text = find_store.text.find(f => f.dirty === text)
    }
    if (lib_vconv.isAbsent(find_text)) {
        find_text = find_store.text.find(f => f.key === text)
    }
    if (lib_vconv.isAbsent(find_text)) return undefined

    return this.translate(this.language, find_text.key)
}

/**
 * @private
 * @param {type_store_text} store_text
 * @param {string|string[]} replaces
 * @param {string} left
 * @param {string} right
 * @returns {string}
 */
function format (store_text, replaces, left, right) {
    let quality = store_text.quality
    if (!lib_vconv.isEmpty(quality)) {
        return lib_vconv.formatExt(quality, replaces, left, right)
    }

    let dirty = store_text.dirty
    if (!lib_vconv.isEmpty(dirty)) {
        return lib_vconv.formatExt(dirty, replaces, left, right)
    }

    return lib_vconv.formatExt(store_text.key, replaces, left, right)
}

/**
 * @private
 * @param {string|string[]} tags
 * @returns {string[]}
 */
function get_tags (tags) {
    if (lib_vconv.isAbsent(tags)) return []

    if (Array.isArray(tags)) {
        let tags_arr = []
        tags.forEach(item => {
            let maybe_tag = lib_vconv.toString(item)
            if (!lib_vconv.isEmpty(maybe_tag)) {
                tags_arr.push(maybe_tag.trim().toLowerCase())
            }
        })
        return tags_arr
    }

    let maybe_tag = lib_vconv.toString(tags)
    if (!lib_vconv.isEmpty(maybe_tag)) {
        return [maybe_tag]
    }

    return []
}

/**
 * @private
 * @param {type_store_text} store_text
 * @param {string[]} tags
 * @returns {boolean}
 */
function add_tags (store_text, tags) {
    if (lib_vconv.isAbsent(store_text)) return false
    if (lib_vconv.isAbsent(tags) || tags.length <= 0) return false
    if (lib_vconv.isAbsent(store_text.tags)) store_text.tags = []
    let added = false
    tags.forEach(t => {
        if (!store_text.tags.some(f => f === t)) {
            store_text.tags.push(t)
            added = true
        }
    })
    return added
}

/**
 * @private
 * @param {string} path
 * @param {string} language
 * @return {string}
 */
function build_file_name(path, language) {
    return lib_path.join(path,'translate.'.concat(language,'.','json'))
}

/**
 * @private
 * @param {string} path
 * @param {type_store} store
 * @param {boolean} allow_tags
 */
function save(path, store, allow_tags) {
    createPath(path)
    let file_name = build_file_name(path, store.language)

    lib_fs.writeFileSync(
        file_name,
        JSON.stringify(
            store.text.map(f => {return {key: f.key, dirty: f.dirty, quality: f.quality, last_read_datetime: f.last_read_datetime, tags: (allow_tags === true && !lib_vconv.isAbsent(f.tags) && f.tags.length > 0 ? f.tags : undefined)}}),
            null,
            '\t'
        )
    )
}

/**
 * @private
 * @param {string} path
 * @param {string} language
 * @returns {type_store}
 */
function load(path, language) {
    createPath(path)
    let file_name = build_file_name(path, language)
    if (lib_fs.existsSync(file_name)) {
        let loaded = JSON.parse(lib_fs.readFileSync(file_name).toString())
        if (lib_vconv.isAbsent(loaded)) {
            return {language: language, text: []}
        } else {
            let text = []
            if (Array.isArray(loaded)) {
                loaded.forEach(l => {
                    if (!lib_vconv.isEmpty(l.key)) {
                        text.push({
                            key: l.key,
                            dirty: lib_vconv.toString(l.dirty, ''),
                            quality: lib_vconv.toString(l.quality, ''),
                            last_read_datetime: lib_vconv.toString(l.last_read_datetime, ''),
                            tags: l.tags
                        })
                    }
                })
            }
            return {
                language: language,
                text: text
            }
        }
    } else {
        return {language: language, text: []}
    }
}

/**
 * @private
 * @param {string} path
 */
function createPath(path)
{
    if (!lib_vconv.isEmpty(path)) {
        if (!lib_fs.existsSync(path)) {
            lib_fs.mkdirSync(path)
        }
    }
}

/**
 * @private
 * @param {Translator} self
 * @param {number} timeout
 */
function go_timeout_save(self, timeout) {
    let true_timeout = lib_vconv.toInt(timeout)
    if (true_timeout <= 0) return
    let timer = setTimeout(function tick() {
        try {
            self.store.forEach(store => {
                save(self.path, store, (self.language === store.language ? true : false))
            })
        } catch (error) {
            self.emit('error', error)
        }
        timer = setTimeout(tick, timeout * 1000 * 60);
    }, timeout * 1000 * 60)
}