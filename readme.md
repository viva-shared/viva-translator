<a name="Translator"></a>

## Translator
(license MIT) simple translator, full example - see example.js

**Kind**: global class  

* [Translator](#Translator)
    * [.init(language, [path], [left], [right], [timeout_save])](#Translator+init) ⇒ <code>string</code>
    * [.translate(language, text, [replaces], [tags])](#Translator+translate) ⇒ <code>string</code>
    * [.translateExt(language, text, [tags])](#Translator+translateExt) ⇒ <code>string</code>
    * [.translate_back(language, text)](#Translator+translate_back) ⇒ <code>string</code>

<a name="Translator+init"></a>

### translator.init(language, [path], [left], [right], [timeout_save]) ⇒ <code>string</code>
initialization translator, set root path for storage text translate files

**Kind**: instance method of [<code>Translator</code>](#Translator)  
**Returns**: <code>string</code> - full path name for storage text translate files  

| Param | Type | Description |
| --- | --- | --- |
| language | <code>string</code> | base (main) language |
| [path] | <code>string</code> | root path for text translate files, if empty - set __dirname/translate |
| [left] | <code>string</code> | left border for mark non-translate substring, if empty - "{" |
| [right] | <code>string</code> | right border for mark non-translate substring, if empty - "}" |
| [timeout_save] | <code>number</code> | timeout (in minutes) save translate data to disk, need for save last_read_datetime, default - "60" |

<a name="Translator+translate"></a>

### translator.translate(language, text, [replaces], [tags]) ⇒ <code>string</code>
translate text

**Kind**: instance method of [<code>Translator</code>](#Translator)  

| Param | Type | Description |
| --- | --- | --- |
| language | <code>string</code> | translate to this language |
| text | <code>string</code> | text for translate |
| [replaces] | <code>string</code> \| <code>Array.&lt;string&gt;</code> | substitutions |
| [tags] | <code>string</code> \| <code>Array.&lt;string&gt;</code> | tags for phrase |

<a name="Translator+translateExt"></a>

### translator.translateExt(language, text, [tags]) ⇒ <code>string</code>
translate text with auto-check replaces

**Kind**: instance method of [<code>Translator</code>](#Translator)  

| Param | Type | Description |
| --- | --- | --- |
| language | <code>string</code> | translate to this language |
| text | <code>string</code> | text for translate |
| [tags] | <code>string</code> \| <code>Array.&lt;string&gt;</code> | tags for phrase |

<a name="Translator+translate_back"></a>

### translator.translate\_back(language, text) ⇒ <code>string</code>
find base language text by text in secondary language

**Kind**: instance method of [<code>Translator</code>](#Translator)  

| Param | Type |
| --- | --- |
| language | <code>string</code> | 
| text | <code>string</code> | 

